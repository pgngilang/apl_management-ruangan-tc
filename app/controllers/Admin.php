<?php

class Admin extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	/*
		fungsi untuk TU melakukan finalisasi peminjaman
	*/
	public function finalisasi()
	{
		//dapatkan input petubahan status
		//dapatkan input id peminjaman
		//panggil fungsi model untuk update status peminjaman
		//dapatkan kode sukses
		//panggil view untuk menampilkan list peminjaman dengan pesan sukses finalisasi
	}

	public function melihat_list()
	{
		/*
		fungsi untuk TU melakukan dapat melihat list siapa saja yang pinjam
	*/
	}

	public function menambah_ruang()
	{
		/*
		fungsi untuk TU menambah ruangan
	*/
	}

	public function edit_ruang()
	{
		/*
		fungsi untuk TU mengedit ruangan
	*/
	}

	public function hapus_ruang()
	{
		/*
		fungsi untuk TU menghapus ruangan
	*/
	}

	public function edit_akun()
	{
		/*
		fungsi untuk TU mengedit akun dosen
	*/
	}

}
