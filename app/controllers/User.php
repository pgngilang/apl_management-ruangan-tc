<?php

use Request;
use PenggunaanPrasarana;

class User extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	/*
		fungsi untuk mengupload dokumen peminjaman
		parameter $id adalah id peminjaman
	*/
	public function upload_dokumen_peminjaman($id)
	{
		//dapatkan input file dokumen peminjaman
		//generate upload file
		//input ke database lewat model dengan parameter $id
		//dapatkan nilai sukses
		//menuju view sukses upload
	}

	/*
		fungsi untuk mengupload dokumentasi
		parameter $id adalah id peminjaman
	*/
	public function upload_dokumen_peminjaman($id)
	{
		//dapatkan input file dokumen peminjaman
		//generate upload file
		//input ke database lewat model dengan parameter $id
		//dapatkan nilai sukses
		//menuju view sukses upload
	}
	public function submit_form()
	{
		//simpan ke variabel
/*		$form = new Form;
		$form->username = Input::get('username');
		$form->idJenisKegiatan = Input::get('id_jns_kegiatan');
		$form->idSemester = Input::get('id_smt');
		$form->namaKegiatan = Input::get('nm_keg');
		$form->deskripsiKegiatan = Input::get('desk_keg');
		$form->waktuMulai = Input::get('wkt_mulai');
		$form->waktuSelesai = Input::get('wkt_selesai');
		$form->save();*/

		//standard
		$data = Input::all();
		$insert = DB::table('kegiatan')->insertGetId(array(
			'username'		=> $data['username'], //yang kanan didalam kurung adalah name di view
			'id_keg'		=> $data['id_kegiatan'], 
			'id_smt'		=> $data['id_smt'],
			'id_jns_kegiatan'		=> $data['id_jns_kegiatan'], 
			'nm_keg'		=> $data['nm_keg'],  
			'desk_keg'		=> $data['desk_kegiatan'], 
			'persetujuan'		=> $data['persetujuan'],
			'tgl_setuju'		=> $data['tgl_setuju']
			//diisi variabel yang ada di tabel. tabelnya yang didatabase blm sesuai btw.
		));		

		//manggil fungsi dari model
		// $insert = PenggunaanPrasarana::isiForm($data['username'],$data['id_jns_kegiatan']);

		//pilih salah satu antara standard atau manggi fungsi

		//redirect
		return View::make("site.view"); //site login diisi view nya
	}
	public function status_ruangan()
	{
		//ngambil data
		$PenggunaanPrasarana = PenggunaanPrasarana::get();

		//redirect
		return View::make("site.view",compact('PenggunaanPrasarana')); //site login diisi view nya
	}

}
