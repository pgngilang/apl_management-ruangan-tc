<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class Dokumen extends Eloquent implements UserInterface, RemindableInterface {
	protected $table = 'Dokumen';


	public function jenisdokumen(){
		return $this->belongsTo('JenisDokumen', 'idJenisDokumen');
	}
}
