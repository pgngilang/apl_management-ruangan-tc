<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class JenisDokumen extends Eloquent implements UserInterface, RemindableInterface {
	protected $table = 'JenisDokumen';


	public function dokumen(){
		return $this->hasMany('Dokumen', 'idJenisDokumen');
	}
}
