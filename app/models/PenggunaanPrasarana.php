<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class PenggunaanPrasarana extends Eloquent implements UserInterface, RemindableInterface {
	

	public function isiForm($username, $idJenisKegiatan, $idSemester, $namaKegiatan, $deskripsiKegiatan, $waktuMulai, $waktuSelesai)
	{
		if(checkUserIsActive($username)){
			DB::table('kegiatan')->insert(
	    		['username' => $username, 'id_jns_kegiatan' => $idJenisKegiatan,
	    			'id_smt' => $idSemester, 'nm_keg' => $namaKegiatan,
	    			'desk_keg' => $deskripsiKegiatan]);
			//sukses input form
			return true;
		}else 
			//gagal input form
			return false;
	}

	private function inputPenggunaanPrasarana()

	//created by DIDIT SEPIYANTO
	//buat ngecek apakah user aktif atau tidak aktif
	private function checkUserIsActive($username){
		$user = DB::table('user')->where('username', $username)->first();
		if($user->is_active != NULL)
			return true;
		else
			return false;
	}

	//created by DIDIT SEPIYANTO
	//buat menginputkan dokumen baru
	public function inputDokumen($idJenisDokumen, $deskripsiDokumen, $fileContent, $mediaType, $fileName, $waktuUnggah){
			DB::table('dokumen')->insert(
	    		['id_jns_dok' => $idJenisDokumen, 'desk_dok' => $deskripsiDokumen,
	    			'file_content' => $fileContent, 'media_type' => $mediaType,
	    			'file_name' => $fileName, 'wkt_unggah' => $waktuUnggah]);
	}

	


}
