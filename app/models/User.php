<?php

use Carbon\Carbon;

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	$public function roles()
	{
		return $this->belongsToMany('role','user_role'); // membuat tabel role has many ke tabel user role
	}
	/**
	 * find out out User is an user role, base on if has any roles 
	 *
	 * @return boolean
	 */

	$public function isUser()
	{
		$roles = $this->role->toArray();  // 
		return !empty($role);
	}
	/**
	 * find out if user has a specific role
	 *
	 * @return boolean
	 */

	public function hasRole($check)
	{
		return in_array($check, array_fetch($this->role->toArray(), 'nm_role'));
	}
	/**
	 * get key in array with corresponding value
	 *
	 * @return int
	 */
	private function getIdInArray ($array, $term)
	{
		foreach ($array as $key => $value) {
			if ($value == $term)
			{
				return $key;
			}
		}
	}

	//Add roles to user to make them a concierge

	public function makeRole($title)
	{
		$assigned_roles = array();
		$roles = array_fetch(role::all()->toArray(), 'nm_role');

		switch ($title) {
			case 'mahasiswa':
				$assigned_roles[] = $this->getInArray($roles,'mahasiswa');
			case 'dosen':
				$assigned_roles[] = $this->getInArray($roles,'dosen');
			case 'tu':
				$assigned_roles[] = $this->getInArray($roles,'tu');
				break;
			
			default:
				# code...
				break;
		}
		$this->roles()->attach($assigned_roles);
	}



	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}
	
	public function getRememberToken()
	{
		return $this->remember_token;
	}

	/**
	 * Set the token value for the "remember me" session.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
		return $this->token_name;
	}

	public function update_password($username, $new_password)
	{
		$mytime = Carbon\Carbon::now();

		DB::table('users')
            ->where('username', $username)
            ->update(['password' => $new_password, 'update_at' => $mytime]);
	}

}
